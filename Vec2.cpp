#include "Vec2.h"
#include <math.h>

Vec2::Vec2()
{

}

Vec2::Vec2(float xin, float yin)
    : x(xin), y(yin)
{

}

Vec2 Vec2::operator + (const Vec2 & rhs) const
{
    return Vec2(this->x + rhs.x, this->y + rhs.y);
}

Vec2 Vec2::operator - (const Vec2 & rhs) const
{
    return Vec2(this->x - rhs.x, this->y - rhs.y);
}

Vec2 Vec2::operator / (const float & val) const
{
    return Vec2(this->x / val, this->y / val);
}

Vec2 Vec2::operator * (const float & val) const
{
    return Vec2(this->x * val, this->y * val);
}

bool Vec2::operator == (const Vec2 & rhs) const
{
    return this->x == rhs.x && this->y == rhs.y;
}

bool Vec2::operator != (const Vec2 & rhs) const
{
    return this->x != rhs.x || this->y != rhs.y;
}

void Vec2::operator += (const Vec2 & rhs)
{
    this->x += rhs.x;
    this->y += rhs.y;
}

void Vec2::operator -= (const Vec2 & rhs)
{
    this->x -= rhs.x;
    this->y -= rhs.y;
}

void Vec2::operator *= (const float & val)
{
    this->x *= val;
    this->y *= val;
}

void Vec2::operator /= (const float & val)
{
    this->x /= val;
    this->y /= val;
}

float Vec2::dist(const Vec2 & rhs) const
{
    return sqrt(pow(this->x - rhs.x, 2.0f) + pow(this->y - rhs.y, 2.0f));
}

float Vec2::dot(const Vec2 & rhs) const
{
    return this->x * rhs.x + this->y * rhs.y;
}

float Vec2::magnitude() const
{
    return sqrt(pow(this->x, 2.0f) + pow(this->y, 2.0f));
}

Vec2 Vec2::normalize() const
{
    return Vec2(this->x / this->magnitude(), this->y / this->magnitude());
}