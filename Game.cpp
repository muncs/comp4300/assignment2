/**
 * COMP 4300 - Assignment 2
 *
 * Created by:
 *      - Devin Marsh - 201239464
 *      - Justin Delaney - 201222684
 *
 * Our special is a giant bullet that when it collides with an enemy, shoots out many smaller bullets.
 * It creates 2 * enemyNumOfSides bullets in a outward circle. We call it 'fireworks'.
 */


#include "Game.h"
#include <math.h>

Game::Game(const std::string & config)
{
    init(config);
}

void Game::init(const std::string & path)
{
    //Read in config
    std::ifstream fin("config.txt");
    std::string token;

    srand(time(0)); // Seed for rand()

    //Read and set configs
    while (fin.good())
    {
        fin >> token;

        if (token == "Window")
        {
            unsigned int _width, _height, _framerate, _fullscreen;
            fin >> _width >> _height >> _framerate >> _fullscreen;

            //Set up default window parameters
            if (_fullscreen)
            {
                m_window.create(sf::VideoMode(_width, _height), "Assignment 2", sf::Style::Fullscreen);
            }
            else
            {
                m_window.create(sf::VideoMode(_width, _height), "Assignment 2");
            }
            m_window.setFramerateLimit(_framerate);
        }
        else if (token == TAG_PLAYER)
        {
            int _sr, _cr, _fr, _fg, _fb, _or, _og, _ob, _ot, _v;
            float _s;

            fin >> _sr >> _cr >> _s >> _fr >> _fg >> _fb >> _or >> _og >> _ob >> _ot >> _v;

            m_playerConfig = PlayerConfig();
            m_playerConfig.SR = _sr;        //Shape Radius
            m_playerConfig.CR = _cr;        //Collision Radius
            m_playerConfig.S = _s;          //Speed
            m_playerConfig.FR = _fr;        //Fill Color Red
            m_playerConfig.FG = _fg;        //Fill Color Green
            m_playerConfig.FB = _fb;        //Fill Color Blue
            m_playerConfig.OR = _fr;        //Outline Color Red
            m_playerConfig.OG = _og;        //Outline Color Green
            m_playerConfig.OB = _ob;        //Outline Color Blue
            m_playerConfig.OT = _ot;        //Outline Thickness
            m_playerConfig.V = _v;          //Shape Vertices
        }
        else if (token == TAG_BULLET)
        {
            int _sr, _cr, _fr, _fg, _fb, _or, _og, _ob, _ot, _v, _l;
            float _s;

            fin >> _sr >> _cr >> _s >> _fr >> _fg >> _fb >> _or >> _og >> _ob >> _ot >> _v >> _l;

            m_bulletConfig = BulletConfig();
            m_bulletConfig.SR = _sr;        //Shape Radius
            m_bulletConfig.CR = _cr;        //Collision Radius
            m_bulletConfig.S = _s;          //Speed
            m_bulletConfig.FR = _fr;        //Fill Color Red
            m_bulletConfig.FG = _fg;        //Fill Color Green
            m_bulletConfig.FB = _fb;        //Fill Color Blue
            m_bulletConfig.OR = _fr;        //Outline Color Red
            m_bulletConfig.OG = _og;        //Outline Color Green
            m_bulletConfig.OB = _ob;        //Outline Color Blue
            m_bulletConfig.OT = _ot;        //Outline Thickness
            m_bulletConfig.V = _v;          //Shape Vertices
            m_bulletConfig.L = _l;          //Lifespan
        }

        else if (token == TAG_ENEMY)
        {
            int _sr, _cr, _or, _og, _ob, _ot, _vmin, _vmax, _l, _si;
            float _smin, _smax;

            fin >> _sr >> _cr >> _smin >> _smax >> _or >> _og >> _ob >> _ot >> _vmin >> _vmax >> _l >> _si;

            m_enemyConfig = EnemyConfig();
            m_enemyConfig.SR = _sr;         //Shape Radius
            m_enemyConfig.CR = _cr;         //Collision Radius
            m_enemyConfig.SMIN = _smin;     //Min Speed
            m_enemyConfig.SMAX = _smax;     //Max Speed
            m_enemyConfig.OR = _or;         //Outline Color Red
            m_enemyConfig.OG = _og;         //Outline Color Green
            m_enemyConfig.OB = _ob;         //Outline Color Blue
            m_enemyConfig.OT = _ot;         //Outline Thickness
            m_enemyConfig.VMIN = _vmin;     //Min Vertices
            m_enemyConfig.VMAX = _vmax;     //Max Vertices
            m_enemyConfig.L = _l;           //Small Lifespan
            m_enemyConfig.SI = _si;         //Spawn Interval
        }

        else if (token == "Font")
        {
            std::string fontFile;
            int _size, _r, _g, _b;      //TODO Make them sf::Uint8 instead?

            fin >> fontFile >> _size >> _r >> _g >> _b;

            if (!m_font.loadFromFile(fontFile))
            {
                //If we can't load the font, print an error to the error console and exit
                std::cerr << "Could not load font!\n";
                exit(-1);
            }

            m_text.setFont(m_font);
            m_text.setColor(sf::Color(_r, _g, _b));
            m_text.setCharacterSize(_size);
            m_text.setPosition(10, 10);
        }
        else
        {
            std::cout << "Bad!\n";
        }
    }

    spawnPlayer();              //Spawn player
    m_clock = sf::Clock();      //Start clock
}

void Game::run()
{
    while (m_running)
    {
        //On pause, keep rendering and allow user to unpause
        if (m_paused)
        {
            sUserInput();
            sRender();
        }
        else
        {
            m_entities.update();
            sEnemySpawner();
            sMovement();
            sCollision();
            sUserInput();
            sRender();
        }
    }
}

void Game::setPaused(bool paused)
{
    m_paused = paused;
}

/**
 * Respawn player in center of screen
 */
void Game::spawnPlayer()
{
    // We create every entity by calling EntityManager.addEntity(tag)
    // This returns a std::shared_ptr<Entity>, so we use 'auto' to save typing
    auto player = m_entities.addEntity(TAG_PLAYER);

    //Add transform
    player->cTransform = new CTransform(
            Vec2(m_window.getSize().x/2.0f, m_window.getSize().y/2.0f),
            Vec2(m_playerConfig.S, m_playerConfig.S),
            0);

    //Add shape
    player->cShape = new CShape(
            m_playerConfig.SR,
            m_playerConfig.V,
            sf::Color(m_playerConfig.FR, m_playerConfig.FG, m_playerConfig.FB),
            sf::Color(m_playerConfig.OR, m_playerConfig.OG, m_playerConfig.OB),
            m_playerConfig.OT);

    //Add collision`
    player->cCollision = new CCollision(m_playerConfig.CR);

    //Add input
    player->cInput = new CInput();

    // Since we want this Entity to be our player, set our Game's player variable to be this Entity
    m_player = player;
}

/**
 * Spawn enemy at a random position
 */
void Game::spawnEnemy()
{
    auto enemy = m_entities.addEntity(TAG_ENEMY);

    enemy->cTransform = new CTransform(
            Vec2((rand() % (m_window.getSize().x - 1) + 1),
                 rand() % (m_window.getSize().y - 1) + 1),
            Vec2(((float)rand() / RAND_MAX) * (m_enemyConfig.SMIN - m_enemyConfig.SMAX) + m_enemyConfig.SMIN,
                 ((float)rand() / RAND_MAX) * (m_enemyConfig.SMIN - m_enemyConfig.SMAX) + m_enemyConfig.SMIN),
            0); // Not 100% sure on the random floats for speed

    enemy->cShape = new CShape(
            m_enemyConfig.SR,
            rand() % (m_enemyConfig.VMAX - m_enemyConfig.VMIN + 1) + m_enemyConfig.VMIN,
            sf::Color(rand() % (0 + 255), rand() % (0 + 255), rand() % (0 + 255)), // Generates a random R, G and B value to fill enemy shapes
            sf::Color(m_enemyConfig.OR, m_enemyConfig.OG, m_enemyConfig.OB),
            m_enemyConfig.OT);

    enemy->cCollision = new CCollision(m_enemyConfig.CR);

    enemy->cScore = new CScore(enemy->cShape->circle.getPointCount() * 100);
}

/**
 * Spawn 2 * number of sides smaller enemies that appear when an enemy is destroyed. Will fade out over time
 * @param e
 */
void Game::spawnSmallEnemies(std::shared_ptr<Entity> e)
{
    //Spawn small enemies only on enemy entities
    if (e->tag() != TAG_ENEMY)
    {
        return;
    }

    const auto angle = (2 * M_PI) / e->cShape->circle.getPointCount();
    const auto speed = e->cTransform->speed.magnitude();

    //Spawn one small enemy per enemy side
    for (auto i = 1; i <= e->cShape->circle.getPointCount(); i++)
    {
        auto small = m_entities.addEntity(TAG_SMALL);

        small->cTransform = new CTransform(
                Vec2(e->cTransform->pos.x, e->cTransform->pos.y),
                Vec2(speed * cos(i * angle), speed * sin(i * angle)),
                0);

        small->cShape = new CShape(
                e->cShape->circle.getRadius() / 2.0f,
                e->cShape->circle.getPointCount(),
                e->cShape->circle.getFillColor(),
                e->cShape->circle.getOutlineColor(),
                e->cShape->circle.getOutlineThickness());

        small->cCollision = new CCollision(e->cCollision->radius / 2.0f);

        small->cLifespan = new CLifespan(m_enemyConfig.L);

        small->cScore = new CScore(small->cShape->circle.getPointCount() * 200);
    }
}

//Used to determine what bullet type is being created
bool isSpecial = false;
/**
 * Fires a bullet from the entity's position towards the target. Depending on isSpecial, will tag it as a 'bullet' or 'special'
 * @param entity
 * @param target
 */
void Game::spawnBullet(std::shared_ptr<Entity> entity, const Vec2 & target)
{
    std::shared_ptr<Entity> bullet;
    float multiplier;
    if (isSpecial)
    {
        bullet = m_entities.addEntity(TAG_SPECIAL);
        multiplier = 2.0f;
    }
    else
    {
        bullet = m_entities.addEntity(TAG_BULLET);
        multiplier = 1.0f;
    }

    Vec2 spawnPoint = entity->cTransform->pos;

    //Add transform
    bullet->cTransform = new CTransform(
            spawnPoint,
            (target - spawnPoint).normalize() * m_bulletConfig.S,
            0);

    //Add shape
    bullet->cShape = new CShape(
            m_bulletConfig.SR * multiplier,
            m_bulletConfig.V * multiplier,
            sf::Color(m_bulletConfig.FR, m_bulletConfig.FG, m_bulletConfig.FB),
            sf::Color(m_bulletConfig.OR, m_bulletConfig.OG, m_bulletConfig.OB),
            m_bulletConfig.OT);

    //Add collision
    bullet->cCollision = new CCollision(m_bulletConfig.CR * multiplier);

    //Add lifespan
    bullet->cLifespan = new CLifespan(m_bulletConfig.L);
}

/**
 * When a special hits an enemy, split into 2 * enemy sides number of bullets and fire in a outward circle
 * @param entity The entity hit
 */
void Game::spawnSpecialWeapon(std::shared_ptr<Entity> entity)
{
    const auto numBullets = 2 * entity->cShape->circle.getPointCount();
    const auto angle = (2 * M_PI) / numBullets;
    for (auto i = 1; i <= numBullets; i++)
    {
        auto bullet = m_entities.addEntity(TAG_BULLET);

        bullet->cTransform = new CTransform(
                Vec2(entity->cTransform->pos.x, entity->cTransform->pos.y),
                Vec2(m_bulletConfig.S * cos(i * angle), m_bulletConfig.S * sin(i * angle)),
                0);

        //Add shape
        bullet->cShape = new CShape(
                m_bulletConfig.SR,
                m_bulletConfig.V,
                sf::Color(m_bulletConfig.FR, m_bulletConfig.FG, m_bulletConfig.FB),
                sf::Color(m_bulletConfig.OR, m_bulletConfig.OG, m_bulletConfig.OB),
                m_bulletConfig.OT);

        //Add collision
        bullet->cCollision = new CCollision(m_bulletConfig.CR);

        //Add lifespan
        bullet->cLifespan = new CLifespan(m_bulletConfig.L);
    }

}

void Game::sMovement()
{
    for (auto & entity : m_entities.getEntities()) {
        //Only update entities with a transform
        if (entity->cTransform) {

            //Handle player movement
            if (entity->tag() == TAG_PLAYER)
            {
                auto pos = m_player->cTransform->pos;
                auto radius = m_player->cCollision->radius;

                //Player does not move unless explicitly given input
                m_player->cTransform->speed = Vec2(0, 0);

                //Vertical movement
                if (m_player->cInput->up && !m_player->cInput->down && pos.y - radius >= 0)                                 //Move up only if below top window bound
                {
                    m_player->cTransform->speed.y = -1.0f * m_playerConfig.S;
                }
                else if (m_player->cInput->down && !m_player->cInput->up && pos.y + radius <= m_window.getSize().y)         //Move down only if above bottom window bound
                {
                    m_player->cTransform->speed.y = m_playerConfig.S;
                }

                //Horizontal movement
                if (m_player->cInput->left && !m_player->cInput->right && pos.x - radius >= 0)                              //Move left only if to the right of left window bound
                {
                    m_player->cTransform->speed.x = -1.0f * m_playerConfig.S;
                }
                else if (m_player->cInput->right && !m_player->cInput->left && pos.x + radius <= m_window.getSize().x)      //Move right only if to the left of right window bound
                {
                    m_player->cTransform->speed.x = m_playerConfig.S;
                }
            }
            
            //Update entity position
            entity->cTransform->pos.x += entity->cTransform->speed.x;
            entity->cTransform->pos.y += entity->cTransform->speed.y;
        }
    }
}

void Game::sCollision()
{
    //All entities vs window
    for (auto & entity : m_entities.getEntities())
    {
        //Skip if entity doesn't have a collision or transform component or is player (player collision handled in sMovement)
        if (!entity->cCollision || !entity->cTransform || entity->tag() == TAG_PLAYER)
        {
            continue;
        }

        //Get vectors
        auto entityPos = entity->cTransform->pos;
        auto entityRadius = entity->cCollision->radius;

        //Determine if there was a collision against window
        if (entityPos.x + entityRadius >= m_window.getSize().x)
        {
            if (entity->cTransform->speed.x > 0)
            {
                entity->cTransform->speed.x *= -1;
            }
        }
        else if (entityPos.x - entityRadius <= 0)
        {
            if (entity->cTransform->speed.x < 0)
            {
                entity->cTransform->speed.x *= -1;
            }
        }

        if (entityPos.y + entityRadius >= m_window.getSize().y)
        {
            if (entity->cTransform->speed.y > 0)
            {
                entity->cTransform->speed.y *= -1;
            }
        } else if (entityPos.y - entityRadius <= 0)
        {
            if (entity->cTransform->speed.y < 0)
            {
                entity->cTransform->speed.y *= -1;
            }
        }
    }

    //Enemies vs everything
    for (auto & enemy : m_entities.getEntities(TAG_ENEMY))
    {
        //Skip if entity doesn't have a collision or transform component
        if (!enemy->cCollision || !enemy->cTransform)
        {
            continue;
        }

        //Get vectors
        auto enemyPos = enemy->cTransform->pos;
        auto enemyRadius = enemy->cCollision->radius;

        //Enemy vs player
        if (enemyPos.dist(m_player->cTransform->pos) <= enemyRadius + m_player->cCollision->radius)
        {
            enemy->destroy();
            m_player->destroy();
            spawnPlayer();
            m_score = 0;
        }

        //Enemies vs bullets
        for (auto & bullet : m_entities.getEntities(TAG_BULLET))
        {
            auto bulletPos = bullet->cTransform->pos;
            auto bulletRadius = bullet->cCollision->radius;

            if (enemyPos.dist(bulletPos) <= enemyRadius + bulletRadius)
            {
                //Spawn small enemies, destroy enemy and bullet, increment score
                spawnSmallEnemies(enemy);
                enemy->destroy();
                bullet->destroy();
                m_score += enemy->cScore->score;
            }
        }

        //Enemies vs special
        for (auto & special : m_entities.getEntities(TAG_SPECIAL))
        {
            auto specialPos = special->cTransform->pos;
            auto specialRadius = special->cCollision->radius;

            if (enemyPos.dist(specialPos) <= enemyRadius + specialRadius)
            {
                //Spawn special weapon, destroy enemy and bullet, increment score (including potential value of smaller enemies)
                spawnSpecialWeapon(enemy);
                enemy->destroy();
                special->destroy();
                m_score += enemy->cScore->score + pow(enemy->cShape->circle.getPointCount(), 2.0f) * 200;
            }
        }
    }

    //Small enemies vs everything
    for (auto & small : m_entities.getEntities(TAG_SMALL))
    {
        //Skip if entity doesn't have a collision or transform componEnemiesent
        if (!small->cCollision || !small->cTransform)
        {
            continue;
        }

        //Get vectors
        auto smallPos = small->cTransform->pos;
        auto smallRadius = small->cCollision->radius;

        //Small vs player
        if (smallPos.dist(m_player->cTransform->pos) <= smallRadius + m_player->cCollision->radius)
        {
            //Destroy player and enemy, respawn player, set score to 0
            small->destroy();
            m_player->destroy();
            spawnPlayer();
            m_score = 0;
        }

        //Small enemies vs bullets
        for (auto & bullet : m_entities.getEntities(TAG_BULLET))
        {
            auto bulletPos = bullet->cTransform->pos;
            auto bulletRadius = bullet->cCollision->radius;

            if (smallPos.dist(bulletPos) <= smallRadius + bulletRadius)
            {
                //Destroy small enemy and bullet, increment score
                small->destroy();
                bullet->destroy();
                m_score += small->cScore->score;
            }
        }

        //Enemies vs special
        for (auto & special : m_entities.getEntities(TAG_SPECIAL))
        {
            auto specialPos = special->cTransform->pos;
            auto specialRadius = special->cCollision->radius;

            if (smallPos.dist(specialPos) <= smallRadius + specialRadius)
            {
                //Destroy small enemy and special, increment score
                small->destroy();
                special->destroy();
                m_score += small->cScore->score;
            }
        }
    }
}

void Game::sEnemySpawner()
{
    //Spawn an enemy every SPAWN_INTERVAL milliseconds
    if (m_clock.getElapsedTime().asMilliseconds() >= m_enemyConfig.SI)
    {
        spawnEnemy();
        m_clock.restart();  //Reset clock so we can countdown to next enemy spawn
    }
}

//Variables to handle background coloring
sf::Clock bgClock = sf::Clock();
const float bgFrequency = 0.0008;
const int bgCenter = 128;
const int bgAmplitude = 127;
const int bgPhase = 2;
void Game::sRender()
{

    //Calculate background color
    float bgSinValue = bgClock.getElapsedTime().asMilliseconds() * bgFrequency;
    int bgR = sin(bgSinValue) * bgAmplitude + bgCenter;
    int bgG = sin(bgSinValue + M_PI / bgPhase) * bgAmplitude + bgCenter;
    int bgB = sin(bgSinValue + 2 * M_PI / bgPhase) * bgAmplitude + bgCenter;

    //Clear screen
    m_window.clear(sf::Color(bgR, bgG, bgB));

    for (auto & entity : m_entities.getEntities()) {

        //Fade out entities that have a lifespan component
        if (entity->cLifespan && !m_paused) {
            const float ratio = entity->cLifespan->clock.getElapsedTime().asMilliseconds() / (float)entity->cLifespan->lifespan;
            if (ratio >= 1.0)
            {
                entity->destroy();
            }
            else
            {
                const auto alpha = (1.0 - ratio) * 255;     //Colors go from 0 to 255

                auto fill = new sf::Color(
                        entity->cShape->circle.getFillColor().r,
                        entity->cShape->circle.getFillColor().g,
                        entity->cShape->circle.getFillColor().b,
                        alpha);

                auto outline = new sf::Color(
                        entity->cShape->circle.getOutlineColor().r,
                        entity->cShape->circle.getOutlineColor().g,
                        entity->cShape->circle.getOutlineColor().b,
                        alpha);

                entity->cShape->circle.setFillColor(*fill);
                entity->cShape->circle.setOutlineColor(*outline);
            }
        }

        //Handle shape rendering
        if (entity->cShape && entity->cTransform) {
            // set the position of the shape based on the entity's transform->pos
            entity->cShape->circle.setPosition(entity->cTransform->pos.x, entity->cTransform->pos.y);

            // set the rotation of the shape based on the entity's transform->angle
            entity->cTransform->angle += 1.0f;
            entity->cShape->circle.setRotation(entity->cTransform->angle);

            // draw the entity's sf::CircleShape
            m_window.draw(entity->cShape->circle);
        }
    }

    m_text.setString(std::to_string(m_score));      //Update score value

    m_window.draw(m_text);                          //Draw score

    m_window.display();                             //Draw window
}

void Game::sUserInput()
{
    sf::Event event;
    while (m_window.pollEvent(event))
    {
        // this event triggers when the window is closed
        if (event.type == sf::Event::Closed || (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape))
        {
            m_running = false;
        }

        // this event is triggered when a key is pressed
        if (event.type == sf::Event::KeyPressed)
        {
            switch (event.key.code)
            {
                case sf::Keyboard::W:
                    std::cout << "W Key Pressed\n";
                    m_player->cInput->up = true;
                    break;
                case sf::Keyboard::S:
                    std::cout << "S Key Pressed\n";
                    m_player->cInput->down = true;
                    break;
                case sf::Keyboard::A:
                    std::cout << "A Key Pressed\n";
                    m_player->cInput->left = true;
                    break;
                case sf::Keyboard::D:
                    std::cout << "D Key Pressed\n";
                    m_player->cInput->right = true;
                    break;
                case sf::Keyboard::P:
                    std::cout << "P Key Pressed\n";
                    setPaused(!m_paused);   //Pause or unpause game when pressed
            }
        }

        if (event.type == sf::Event::KeyReleased)
        {
            switch (event.key.code)
            {
                case sf::Keyboard::W:
                    std::cout << "W Key Released\n";
                    m_player->cInput->up = false;
                    break;
                case sf::Keyboard::S:
                    std::cout << "S Key Released\n";
                    m_player->cInput->down = false;
                    break;
                case sf::Keyboard::A:
                    std::cout << "A Key Released\n";
                    m_player->cInput->left = false;
                    break;
                case sf::Keyboard::D:
                    std::cout << "D Key Released\n";
                    m_player->cInput->right = false;
                    break;
            }
        }

        if (event.type == sf::Event::MouseButtonPressed)
        {
            if (!m_paused)
            {
                if (event.mouseButton.button == sf::Mouse::Left)
                {
                    std::cout << "Left Mouse Button Clicked at (" << event.mouseButton.x << "," << event.mouseButton.y << ")\n";
                    isSpecial = false;
                    spawnBullet(m_player, Vec2(event.mouseButton.x, event.mouseButton.y));      //Spawn normal bullet
                }

                if (event.mouseButton.button == sf::Mouse::Right)
                {
                    std::cout << "Right Mouse Button Clicked at (" << event.mouseButton.x << "," << event.mouseButton.y << ")\n";
                    //Spawn special bullet
                    isSpecial = true;
                    spawnBullet(m_player, Vec2(event.mouseButton.x, event.mouseButton.y));      //Spawn special bullet
                }
            }
        }
    }
}